from django.db import models
from datetime import date, time

from courier.models import Courier
from partner.models import Partner


class Order(models.Model):

    STATUS = (
        ('draft', 'Draft'),
        ('in_the_way', 'In the way'),
        ('done', 'Done'),
        ('cancel', 'Cancel')
    )

    company_number = models.CharField(max_length=100, default='')
    partner_number = models.CharField(max_length=100, default='')
    status = models.CharField(choices=STATUS, max_length=10, default='draft')
    email = models.EmailField(blank=True, null=True)
    comment = models.TextField(default='')

    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')

    phone = models.CharField(max_length=100, default='')
    address = models.CharField(max_length=300, default='')

    date = models.DateField(default=date.today)
    time_start = models.TimeField(blank=True, null=True)
    time_end = models.TimeField(blank=True, null=True)

    courier = models.ForeignKey(Courier, on_delete=models.CASCADE, null=True, related_name='orders')
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE, null=True, related_name='orders')

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name
