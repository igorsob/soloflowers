from rest_framework import serializers

from flowers.email import send_email
from .models import Order


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ('id', 'company_number', 'partner_number', 'status', 'email', 'first_name', 'last_name', 'phone',
                  'address', 'date', 'time_start', 'time_end', 'courier', 'partner', 'full_name', 'comment')

    def update(self, instance, validated_data):

        order = super(OrderSerializer, self).update(instance, validated_data)

        status = validated_data.get('status', False)

        if status in ('done', 'cancel'):
            send_email(status, order)

        return order
