from django.db import models
from django.contrib.auth import get_user_model


class Partner(models.Model):

    name = models.CharField(max_length=100, default='')

    email = models.EmailField()
    phone = models.CharField(max_length=100, default='')

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, null=True, related_name='partner')
