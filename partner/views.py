from rest_framework import viewsets

from .serializer import Partner, PartnerSerializer


class PartnerViewSet(viewsets.ModelViewSet):

    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer
