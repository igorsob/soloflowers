from rest_framework import serializers

from .models import Partner


class PartnerSerializer(serializers.ModelSerializer):

    # full_name = serializers.Field(source='full_name')

    class Meta:
        model = Partner
        fields = ('id', 'name', 'email', 'phone', 'orders')
