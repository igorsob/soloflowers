from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from courier.views import CourierViewSet, CourierSerializer
from partner.views import PartnerViewSet
from order.views import OrderViewSet, OrderSerializer, Order

from django.contrib.auth import get_user_model

from rest_framework import serializers

from rest_framework import viewsets


class UserSerializer(serializers.ModelSerializer):

    # orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True, source='courier.orders')
    orders = OrderSerializer(many=True, read_only=True, source='courier.orders')
    courier = CourierSerializer()

    class Meta:
        model = get_user_model()
        fields = ('id', 'orders', 'courier')


class UserViewSet(viewsets.ModelViewSet):

    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


router = routers.DefaultRouter()
router.register(r'couriers', CourierViewSet)
router.register(r'partners', PartnerViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
