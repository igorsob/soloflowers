from django.core.mail import send_mail


def send_email(status, order):
    number = order.partner_number or order.id
    if status == 'done':
        message = f'Заказ номер {number}, по адресу {order.address} доставлен.'
    else:
        message = f'Заказ номер {number}, по адресу {order.address} не доставлен.'\
                  f'{" Причина: " + order.comment if order.comment != "" else ""}'
    subject = f'Заказ номер {number}'
    from_email = 'soloflowerszakaz@gmail.com'
    recipient_list = [order.partner.email]

    send_mail(subject, message, from_email, recipient_list)
