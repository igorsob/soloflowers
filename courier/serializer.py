from rest_framework import serializers

from .models import Courier


class CourierSerializer(serializers.ModelSerializer):

    # full_name = serializers.Field(source='full_name')

    class Meta:
        model = Courier
        fields = ('id', 'first_name', 'last_name', 'phone', 'full_name', 'orders', 'user')
