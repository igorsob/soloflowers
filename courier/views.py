from rest_framework import viewsets

from .serializer import Courier, CourierSerializer


class CourierViewSet(viewsets.ModelViewSet):

    queryset = Courier.objects.all()
    serializer_class = CourierSerializer
