from django.db import models
from django.contrib.auth import get_user_model


class Courier(models.Model):

    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')

    phone = models.CharField(max_length=100, default='')

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, null=True, related_name='courier')

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name
